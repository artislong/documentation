[TOC]

# Lombok

[Project Lombok](https://projectlombok.org/) is a java library that automatically plugs into your editor and build tools, spicing up your java.Never write another getter or equals method again. Early access to future java features such as `val`, and much more.

这是lombok官网的解释，大概意思是说lombok项目是一个java库，会自动处理代码的编译，比如你不用写getter方法，它就会自动帮你实现。

下面简单介绍一些在开发中常用的注解。

## Lombok常用注解

### @Getter

可标注到类或属性上，标注到类上表示此类中的所有属性生成getter方法，标注到某个属性上，表示此属性生成getter方法。

### @Setter

和@Getter类似，可标注到类或属性上，标注到类上表示此类中的所有属性生成setter方法，标注到某个属性上，表示此属性生成setter方法。

### @ToString

只能标注到类上，相当于是重写此类的toString方法。

### @EqualsAndHashCode

只能标注到类上，相当于是重写此类的hashCode和equals方法。

### @NoArgsConstructor

只能标注到类上，生成无参的构造方法。

### @Data

只能标注到类上，综合@Getter，@Setter，@ToString，@EqualsAndHashCode，@NoArgsConstructor五个注解的功能。

### @Value

只能标注到类上，综合@Getter，@Setter，@ToString，@EqualsAndHashCode，@NoArgsConstructor五个注解的功能，和@Data不同的是，默认将所有属性定义成final的，也就是只会生成getter方法，不会生成setter方法，如果不需要final，则给属性加上@NonFinal注解即可。

### @AllArgsConstructor

只能标注到类上，生成包含所有属性的构造方法，使用此注解时建议和@NoArgsConstructor结合使用，否则此类将没有无参的构造方法。

### @RequiredArgsConstructor

只能标注到类上，会生成一个包含常量，和标识了@NotNull的变量 的构造方法。生成的构造方法是private，如何想要对外提供使用可以使用staticName选项生成一个static方法。如：

```java
@RequiredArgsConstructor(staticName = "passwd")
public class User {
    @NonNull private String password;
}
```

上面代码编译后对应下面的代码

```java
public class User {
	private User(String password) {
		this.password = password;
	}
    public static User passwd(String password) {
    	return new User(password);
    }
}
```

### @Builder

只能标注到类上，将生成类的一个当前流程的一种链式构造工厂，如下：

```java
User buildUser = User.builder().password("haha").username("gaga").build();
```

可配合@Singular注解使用，@Singular注解使用在jdk内部集合类型的属性，Map类型的属性以及[Guava](https://github.com/google/guava)的`com.google.common.collect` 的属性上。例如 未标注@Singular的属性，一般setter时，会直接覆盖原来的引用，标注了@Singular的属性，集合属性支持添加操作，会在属性原来的基础上增加。

```java
public static class UserBuilder {
        private String username;
        private String password;
        private ArrayList<String> projects;
        UserBuilder() {
        }
        public User.UserBuilder username(String username) {
            this.username = username;
            return this;
        }
        public User.UserBuilder password(String password) {
            this.password = password;
            return this;
        }
        public User.UserBuilder project(String project) {
            if (this.projects == null) this.projects = new ArrayList<String>();
            this.projects.add(project);
            return this;
        }
        public User.UserBuilder projects(Collection<? extends String> projects) {
            if (this.projects == null) this.projects = new ArrayList<String>();
            this.projects.addAll(projects);
            return this;
        }
        public User.UserBuilder clearProjects() {
            if (this.projects != null)
                this.projects.clear();
            return this;
        }
        public User build() {
            Set<String> projects;
            switch (this.projects == null ? 0 : this.projects.size()) {
                case 0:
                    projects = java.util.Collections.emptySet();
                    break;
                case 1:
                    projects = java.util.Collections.singleton(this.projects.get(0));
                    break;
                default:
                    projects = new java.util.LinkedHashSet<String>(this.projects.size() < 1073741824 ? 1 + this.projects.size() + (this.projects.size() - 3) / 3 : Integer.MAX_VALUE);
                    projects.addAll(this.projects);
                    projects = java.util.Collections.unmodifiableSet(projects);
            }
            return new User(username, password, projects);
        }
        public String toString() {
            return "User.UserBuilder(username=" + this.username + ", password=" + this.password + ", projects=" + this.projects + ")";
        }
    }
```

### @Accessors

可标注在类或属性上，当然最实用的功能还是标注到类上。

**标注到类上**，chain属性设置为true时，类的所有属性的setter方法返回值将为this，用来支持setter方法的链式写法。如：

```java
new User().setPassword("gaga").setUsername("haha");
```

fluent属性设置为true时，类的所有getter，setter方法将省略get和set前缀，获取属性值直接使用属性名相同的无参方法，设置属性值使用属性名相同的有参方法，并且返回值为this。如：

```java
User user = new User().password("gaga").username("haha");
String password = user.password();
String username = user.username();
```

**标注到属性上**，使用prefix设置需要省略的属性生成getter，setter方法时的前缀，且属性必须为驼峰式命名。

如：

```java
@Accessors(prefix = "a")
@Getter
@Setter
private String aUsername = "gaga";
```

编译之后为

```java
public String getUsername() {
  	return aUsername;
}
public void setUsername(String aUsername) {
  	this.aUsername = aUsername;
}
```

以上一些常用的lombok的用法介绍完了，在日常的开发或者自己的练习中，使用lombok并结合各版本的jdk特性，将更大的提高开发效率，提高开发质量。









