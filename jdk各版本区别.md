## jdk5新特性

### 1、自动装箱和拆箱
### 2、枚举
### 3、静态导入
### 4、可变参数
### 5、內省
   内省是Java语言对Bean类属性、事件的一种缺省处理方法。例如类A中有属性那么，那我们可以通过getName，setName来得到其值或者设置新的值。通过getName/setName来访问name属性，这就是默认的规则。<!--more-->Java中提供了一套API用来访问某个属性的getter，setter方法，通过这些API可以使你不需要了解这个规则，这些API存放于包java.beans中。<br>
   一般的做法是通过类Introspector来获取某个对象的BeanInfo信息，然后通过BeanInfo来获取属性的描述器（PropertyDescriptor），通过这个属性描述器就可以获取某个属性对应的getter/setter方法，然后我们就可以通过反射机制来调用这些方法。
### 6、泛型
### 7、For-Each循环

## jdk6新特性
### 1、Desktop类和SystemTray类
   AWT新增加了两个雷：Desktop，SystemTray。<p>
   Desktop可以用来打开系统默认浏览器指定的URL，打开系统默认邮件客户端给指定的邮件账号发邮件，用默认应用程序打开或编辑文件（比如，用记事本打开txt文件），用系统默认的打印机打印文档<p>
   SystemTray可以用来在系统托盘区创建一个托盘程序
### 2、使用JAXB2来实现对象与XML之间的映射
   也就是对象与XML之间的映射（OXM），也可以通过XMLBeans和Castor等来实现同样的功能。
### 3、StAX
   StAX是The Streaming API for XML的缩写，一种利用拉模式解析(pull-parsing)XML文档的API.StAX通过提供一种基于事件迭代器(Iterator)的API让 程序员去控制xml文档解析过程,程序遍历这个事件迭代器去处理每一个解析事件，解析事件可以看做是程序拉出来的，也就是程序促使解析器产生一个解析事件 然后处理该事件，之后又促使解析器产生下一个解析事件，如此循环直到碰到文档结束符； <p>
   SAX也是基于事件处理xml文档，但却 是用推模式解析，解析器解析完整个xml文档后，才产生解析事件，然后推给程序去处理这些事件；DOM 采用的方式是将整个xml文档映射到一颗内存树，这样就可以很容易地得到父节点和子结点以及兄弟节点的数据，但如果文档很大，将会严重影响性能。
### 4、使用Compiler API
   使用JDK6的Compiler API去动态的编译Java源文件，Compiler API结合反射功能就可以实现动态的产生Java代码并编译执行这些代码。
### 5、轻量级Http Server API
### 6、插入式注解处理API
### 7、用Console开发控制台程序
### 8、对脚本语言的支持如：ruby，groovy，javascript
### 9、Common Annotations

## jdk7新特性
### 1、switch中可以使用字符串
### 2、泛型的自动判断
### 3、自定义自动关闭类（实现AutoCloseable接口）
### 4、新增一些取环境信息的工具方法（System中的方法）
### 5、Boolean类型反转，空指针安全，参数与位运算
### 6、两个char间的equals
### 7、安全的加减乘除

### 1、对Java集合（Collections）的增强支持

```java
	List<String> list=["item"]; //向List集合中添加元素
	String item=list[0]; //从List集合中获取元素
	Set<String> set={"item"}; //向Set集合对象中添加元
	Map<String,Integer> map={"key":1}; //向Map集合中添加对象
	int value=map["key"]; //从Map集合中获取对象
```
   但是经过自己测试，按照上面的使用方法，并不能创建集合。
### 2、int支持二进制数据
### 3、在try catch异常捕捉中，一个catch可以写多个异常类型

```java
	Connection conn = null;
	try {
    	Class.forName("com.mysql.jdbc.Driver");
    	conn = DriverManager.getConnection("","","");
	} catch(ClassNotFoundException|SQLException ex) {
    	ex.printStackTrace();
	}
```
### 4、try catch中资源定义好之后try catch自动关闭

```java
	try (BufferedReader in  = new BufferedReader(new 	FileReader("in.txt"));
     	BufferedWriter out = new BufferedWriter(new 	FileWriter("out.txt"))) {
		int charRead;
		while ((charRead = in.read()) != -1) {
        	System.out.printf("%c ", (char)charRead);
        	out.write(charRead);
    	}
	} catch (IOException ex) {
    	ex.printStackTrace();
	}
```
## jdk8新特性
### 1、接口的默认方法
Java 8允许我们给接口添加一个非抽象的方法实现，只需要使用default关键字即可，这个特征又叫做扩展方法，示例如下：

```java
	public interface Formula {
    	double calculate(int a);
    	default double sqrt(int a) {
			return Math.sqrt(a);
    	}
	}
```
Formula接口在拥有calculate方法之外同时还定义了sqrt方法，实现了Formula接口的子类只需要实现一个calculate方法，默认方法sqrt将在子类上可以直接使用。

```java
    Formula formula = new Formula() {
		@Override
		public double calculate(int a) {
			return sqrt(a * 100);
        }
    };
    System.out.println(formula.calculate(100));  // 100.0
    System.out.println(formula.sqrt(16));  // 4.0
```
文中的formula被实现为一个匿名类的实例，该代码非常
### 2、Lambda表达式

```java
	List<String> names = Arrays.asList("tom","jace","mike");
	Collections.sort(names, new Comparator<String>() {
		@Override
		public int compare(String o1, String o2) {
			return o2.compareTo(o1);
    	}
	});
```
只需要给静态方法Collections.sort传入一个List对象以及一个比较器来指定顺序排列。通常做法都是创建一个匿名的比较器对象，然后将其传递给sort方法。
在Java 8中提供了更简洁的语法，lambda表达式：

```java
	Collections.sort(names, (String a, String b) -> {
		return b.compareTo(a);
	});
```
还可以更简洁：

```java
	Collections.sort(names, (String a, String b) -> b.compareTo(a));
```
去掉大括号以及return关键字

```java
	Collections.sort(names, (a,b) -> b.compareTo(a));
```
Java编译器可以自动推导出参数类型，所以可以不用再写一次类型。
### 3、函数式接口
Lambda表达式是如何在java的类型系统中表示的呢？<p>
每一个lambda表达式都对应着一个类型，通常是接口类型。而“函数式接口”是指仅仅只包含一个抽象方法的接口，每一个该类型的lambda表达式都会被匹配到这个抽象方法。因为默认方法不算抽象方法，所以也可以给自己的函数式接口添加默认方法。<p>
我们可以将lambda表达式当做一个抽象方法的接口类型，确保自己的接口一定达到这个要求，你只需要给你的接口添加**@FunctionalInterface**注解，编译器如果发现标注了这个注解的接口有多于一个抽象方法的时候就会报错。也就是说**@FunctionalInterface**注解标注的接口只能有一个抽象方法。<p>
例如：

```java
	@FunctionalInterface
	public interface Converter<F, T> {
		T convert(F from);
	}
	Converter<String, Integer> converter = (from) -> Integer.valueOf(from);
	Integer converted = converter.convert("123");
	System.out.println(converted);
```
以上代码不需要@FunctionalInterface注解也是正确的。
### 4、方法与构造函数引用
上面的代码也可以通过静态方法引用来表示：

```java
	Converter<String, Integer> converter = Integer::valueOf;
	Integer converted = converter.convert("123");
	System.out.println(converted);
```
Java8允许使用::关键字来传递方法或者构造函数引用，上面的代码展示了如何引用一个静态方法，我们也可以引用一个对象的方法：

```java
	public class Person {
    	String firstName;
	    String lastName;
    	Person() {
    	}
		public Person(String firstName, String lastName) {
			this.firstName = firstName;
			this.lastName = lastName;
	    }
	}
```
指定一个用来创建Person对象的对象工厂接口：

```java
	public interface PersonFactory<P extends Person> {
		P create(String fisrtName, String lastName);
	}
```
创建Person对象

```java
	PersonFactory<Person> personFactory = Person::new;
	Person person = personFactory.create("Peter","Parker”);
```
我们只需要使用Person::new 来获取Person类构造函数的引用，Java编译器就会自动根据PersonFactory.create方法的签名来选择合适的构造函数，这点感觉有点像C++的写法。

### 5、Lambda作用域
在lambda表达式中访问外层作用域和老版本的匿名对象中的方式很相似。你可以直接访问标记了final的外层局部变量，或者实例的字段以及静态变量。
### 6、访问局部变量
我们可以直接在lambda表达式中访问外层的局部变量

```java
	final int num = 1;
	Converter<Integer, String> stringConverter = (from) -> String.valueOf(from + num);
	stringConverter.convert(2);
```
但是和匿名对象不同的是，这里的变量num可以不用声明为final，该代码同样正确。
### 7、访问对象字段与静态变量
和本地不良不同的是，lambda内部对于实例的字段以及静态变量是即可读又可写。该行为和匿名对象是一致的：

```java
	static int outerStaticNum;
	int outerNum;
	public void testScopes() {
    	Converter stringConverter1 = (from) -> {
        	outerNum = 23;
        	return String.valueOf(from);
    	};
	    Converter stringConverter2 = (from) -> {
    	    outerStaticNum = 72;
        	return String.valueOf(from);
    	};
	}
```
### 8、Date API

重新整理的time相关的类、包，提供对之前的java.util.Date这样的互相转换的方法。

### 9、Annotation注解

主要是可重复注解

## jdk9新特性

[jdk9 官方文档](https://docs.oracle.com/javase/9/whatsnew/toc.htm#JSNEW-GUID-C23AFD78-C777-460B-8ACE-58BE5EA681F6)

安装了jdk9 并看了jdk9的官方文档，发现jdk9与之前的版本相比有很大的改变，包括源码管理，新的版本格式，增加jshell，更多的诊断命令等功能。

### 1、模块化JDK源码

引入了模块化的JAR文件（jmod），删除了1.8之前版本的rt.jar和tools.jar，重构了JDK和JRE的运行时镜像，从而容纳module并提升了性能，安全以及可靠性。

JDK9的模块化源码在JDK家目录下的jmods目录下，像一些我们常用的API，都在java.base.jmod模块下。

在此部分也提供了一些辅助开发的工具

- jmod

可以使用jmod创建jmod文件，查看现有jmod文件内容

~~~
PS D:\java\jdk1.9\jmods> D:\java\jdk1.9\bin\jmod.exe
错误: 必须指定创建, 提取, 列出, 描述或散列之一
用法: jmod (create|extract|list|describe|hash) <选项> <jmod 文件>
使用 --help 列出可能的选项
PS D:\java\jdk1.9\jmods> D:\java\jdk1.9\bin\jmod.exe describe .\java.base.jmod
java.base@9
exports java.io
exports java.lang
exports java.lang.annotation
exports java.lang.invoke
exports java.lang.module
exports java.lang.ref
exports java.lang.reflect
exports java.math
exports java.net
exports java.net.spi
exports java.nio
...
~~~

[详细使用方法](https://docs.oracle.com/javase/9/tools/jmod.htm#JSWOR-GUID-0A0BDFF6-BE34-461B-86EF-AAC9A555E2AE),可以参考Oracle官网文档，或者自己去根据jmod命令的提示参数自己去一个个尝试。

- jdeps -jdkinternals

确定自己的代码是否使用了JDK内部的API

~~~
PS H:\project\test\out\production\classes\com\test\util> D:\java\jdk1.9\bin\jdeps -jdkinternals .\DES.class
DES.class -> D:\java\jdk1.8\jre\lib\rt.jar
   com.fndsoft.util.DES (DES.class)
      -> sun.misc.BASE64Decoder                             JDK internal API (rt.jar)
      -> sun.misc.BASE64Encoder                             JDK internal API (rt.jar)

警告: 不支持 JDK 内部 API, 它们专用于通过不兼容方式来删除
或更改的 JDK 实现, 可能会损坏您的应用程序。
请修改您的代码, 消除与任何 JDK 内部 API 的相关性。
有关 JDK 内部 API 替换的最新更新, 请查看:
https://wiki.openjdk.java.net/display/JDK8/Java+Dependency+Analysis+Tool

JDK Internal API                         Suggested Replacement
----------------                         ---------------------
sun.misc.BASE64Decoder                   Use java.util.Base64 @since 1.8
sun.misc.BASE64Encoder                   Use java.util.Base64 @since 1.8
~~~

jdeps命令在jdk1.8中就已经存在了，不过在jdk9中才真正提上日程。

- jlink

可以组装和优化一个模块到一个正在运行的镜像中。

~~~
D:\java\jdk1.9\bin\jlink.exe --module-path $JAVA_HOME/jmods --add-modules java.naming,jdk.crypto.cryptoki --output mybuild
~~~

总的来说，JDK的一些命令工具都挺简单的，完全可以自己参考命令详情去尝试，看命令工具的执行效果。

### 2、新的版本格式

提供了一个简化的版本字符串格式，从而更加清楚的区分major、minor、security和patch的更新新版本。

### 3、JShell

这个模块提供了java编程语言的代码段的评价工具的支持，如read-eval-print循环（REPL），包括jshell工具。单独的封装，支持新建工具，配置工具的执行，并以编程方式启动现有的java shell工具。

JShell提供了一个交互式的命令行界面，用于评估Java编程语言的声明，语句和表达式。它有助于原型设计和探索编码选项，具有即时的结果和反馈。

- REPL（Read-Eval-Print Loop）

  交互式解释器环境。

  R(read)、E(evaluate)、P(print)、L(loop) 

  输入值，交互式解释器会读取输入内容并对它求值，再返回结果，并重复此过程。

### 4、添加更多的诊断命令

定义了其他诊断命令，以提高诊断热点和JDK问题的能力。

主要命令为[jcmd](https://docs.oracle.com/javase/9/tools/jcmd.htm#JSWOR743)

### 5、删除启动时JRE版本选择

现有的应用程序通常是通过java web start部署（一个JNLP文件），本地操作系统包装系统，或主动安装。这些技术都有自己的方法来管理正在查找或更新下载的JRE，这使得运行时的JRE版本的选择过时。

### 6、多版本JAR文件

扩展JAR文件格式，以便在单个存档中共享多个Java版本的类文件。

多版本JAR包含针对JDK版本的类和资源的附加版本目录。可以使用jar工具--release选项指定版本化目录。

### 7、验证JVM命令行参数

验证JVM命令行参数，以避免出现故障，如果发现它们无效，则会显示相应的错误信息。并且为需要用户指定数值的参数实现了范围和可选约束检查。

### 8、编译旧版本

改进了javac命令，从而使得它可以编译出跑在JDK9之前版本的Java平台上的程序。

当使用--source或者--target命令时，编译出的程序可能偶尔会调用之前版本不支持的API。该--release选项将防止意外使用的API。

### 9、模块化Java应用打包

将Project Jigsaw的功能集成到Java Packager中，包括模块感知和自定义运行时创建。

利用jlink工具创建更小的包，创建仅使用JDK9运行的应用程序，不能用早期版本的JRE来打包。

### 10、分段代码缓存

将代码缓存为不同的片段，其中包含编译一个特定类型的代码，提高性能，便于未来扩展。

### 11、使用G1为默认的垃圾收集器

### 12、太多了，更多详细的特性在以后的工作和学习中在继续了解加深...

