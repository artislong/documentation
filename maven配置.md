---
layout: post
title: maven配置
date: 2016-12-22 00:43:01
toc: true
tags: maven
categories: 随笔
description:
---

## maven打包加载src/main/java中的配置文件

 maven打jar包时，不能识别src/main/java中的配置文件(*.xml,*.properties)，需要在build中添加以下配置<!--more-->：

```xml
	<resources>
    	<resource>
        	<directory>src/main/java/</directory>
	        <includes>
	            <include>**/*.properties</include>
	            <include>**/*.xml</include>
	        </includes>
	        <!-- 是否替换资源中的属性 -->
	        <filtering>false</filtering>
	    </resource>
	    <resource>
	        <directory>src/main/resources</directory>
	        <!--
	        <includes>
	            <include>**/*.properties</include>
	            <include>**/*.xml</include>
	        </includes>
	        <filtering>true</filtering>
	        -->
	    </resource>
	</resources>
```