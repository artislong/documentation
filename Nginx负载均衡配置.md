## Nginx负载均衡配置

​	Nginx ("engine x") 是一个高性能的HTTP和反向代理服务器，也是一个IMAP/POP3/SMTP服务器。Nginx是由Igor Sysoev为俄罗斯访问量第二的Rambler.ru站点开发的，第一个公开版本0.1.0发布于2004年10月4日。其将源代码以类BSD许可证的形式发布，因它的稳定性、丰富的功能集、示例配置文件和低系统资源的消耗而闻名。2011年6月1日，nginx 1.0.4发布。<!--more-->

​	Nginx是一款轻量级的Web 服务器/反向代理服务器及电子邮件（IMAP/POP3）代理服务器，并在一个BSD-like 协议下发行。由俄罗斯的程序设计师Igor Sysoev所开发，供俄国大型的入口网站及搜索引擎Rambler（俄文：Рамблер）使用。其特点是占有内存少，并发能力强，事实上nginx的并发能力确实在同类型的网页服务器中表现较好，中国大陆使用nginx网站用户有：百度、京东、新浪、网易、腾讯、淘宝等。

这段来自百度百科，简单介绍一下，读者若需详细了解可自行查找资料，本文侧重于Nginx负载相关的具体操作配置介绍。



Nginx常用的功能有Http代理、反向代理，负载均衡器，web缓存等功能，最近项目需要做负载，简单的研究了一下nginx，对反向代理和负载均衡着重看了一下，所以接下来的文章主要对这两部分进行介绍。



#### 1、Nginx服务器的安装

windowns版Nginx下载地址：http://nginx.org/en/docs/windows.html

> windows上安装Nginx比较简单，Nginx官方已经提供了打包好的.exe的运行文件，不需要用户自己去编译运行。直接打开上面的地址，下载好windows版的nginx，解压后双击nginx.exe或者在命令窗口运行nginx.exe即可。

> 因为Nginx默认端口是80端口，所以启动成功之后在浏览器地址栏输入localhost就可以看到Nginx的欢迎页面。



linux版的Nginx下载地址：http://nginx.org/

> 下载nginx之前，请确保自己的linux系统已经安装了g++，gcc。因为nginx是纯C语言编写，在linux下安装时需要去编译源码安装。

- 解压Nginx源码包

  ```
  > tar -zxvf nginx-1.11.5.tar.gz
  ```


- 设置一下nginx配置信息

  ```
  > chmod -R 777 nginx-1.11.5
  > cd nginx-1.11.5
  > ./configure --prefix=/usr/local/nginx  #此处设置prefix，是设置nginx的安装路径
  ```

- 编译安装

  ```
  > make && make install #将源码文件编译成可执行文件和各种库文件，并将其复制到上面设置的安装目录中
  ```

- 启动nginx

  ```
  > /usr/local/nginx/sbin/nginx -c /usr/local/nginx/conf/nginx.conf #这一步可以不指定nginx配置文件
  ```

- 还有最重要的一步，打开Nginx的防火墙端口

  ```
  > vi /etc/sysconfig/iptables
  > 添加端口，如： -A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT
  > /etc/init.d/iptables restart #重启防火墙，让修改生效
  ```

- 在自己本机的浏览器中输入localhost就可以看到Nginx的欢迎页面。

#### 2、配置Nginx

​	在真正开始配置之前，先说一下Nginx的配置文件配置的基础知识。（Linux和Windows的配置一样，不分开说了）

	> Nginx的配置文件在安装目录下的conf目录中，一些默认配置都在这个目录下。

> nginx.conf 的注释符号为 #

打开nginx.conf文件，可以大概浏览一下，配置文件基本可以分为几个模块

```nginx
...              #全局块
events {         #events块
   ...
}
http      #http块
{
    ...   #http全局块
    server        #server块
    { 
        ...       #server全局块
        location [PATTERN]   #location块
        {
            ...
        }
        location [PATTERN] 
        {
            ...
        }
    }
    server
    {
      ...
    }
    ...     #http全局块
}
```

- 全局块：配置影像nginx的全局指令。一般有nginx的进程数，错误日志文件路径，nginx的主进程号等
- events块：配置Nginx的工作模式，每个进程的最大连接数等
- http块：可以嵌套多个server，配置代理，缓存，日志等功能以及第三方模块的配置。如文件引入，mime-type定义，连接超时时间，单连接请求数等等
- server块：配置虚拟主机的相关参数，一个http中可以有多个server
- location块：配置请求的路由，以及各种页面的处理情况

给大家附上一个nginx的配置文件，博主在公司的测试环境上搭的一个负载均衡器。

```nginx
#user  nobody;
#nginx进程数，建议设置为CPU总核心数
worker_processes  1;
#错误日志文件路径
error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;
#nginx主进程号
pid        logs/nginx.pid;
#工作模式与连接数上限
events {
	# 单个进程的最大连接数（最大连接数=连接数*进程数）
	worker_connections  1024;
}
http {
	#文件扩展名与文件类型映射表
	include       mime.types;
	#默认文件类型
	default_type  application/octet-stream;
	#日志文件
	access_log  logs/access.log  main;
	#开启高效文件传输模式，sendfile指令指定nginx是否调用sendfile函数来输出文件，对于普通应用设为on，如果用来进行下载等应用磁盘IO重负载应用，可设置为off，以平衡磁盘与网络I/O处理速度，降低系统的负载。注意：如果图片显示不正常把这个改成off。 
	sendfile        on;
	#防止网络阻塞
	#tcp_nopush     on;
	#长连接超时时间，单位是秒
	keepalive_timeout  65;
	#服务器列表名称随便写
	upstream global {
		#upstream的负载均衡，weight是权重，可以根据机器配置定义权重，权值越高被分配到的几率越大。
		server 192.168.100.100:8081; #weight=1
		server 192.168.100.100:8084;
		ip_hash; #upstream的分配方式
	}
	#开启gzip压缩输出
	#gzip  on;
	server {
		#监听端口
		listen       8087;
		#域名可以有多个，用空格隔开
		server_name  192.168.100.100;
		#默认编码
		#charset koi8-r;
		#对“/”启用反向代理
		location / {
			#前端页面项目部署路径
			root   /home/fisCM/nginx/html;
			#默认主页面
			index  index.html index.htm;
		}
		#5xx错误对应的页面
		#error_page   500 502 503 504  /50x.html;
		#请求的url过滤，正则匹配，~为区分大小写，~*为不区分大小写。
		#我们项目的请求路径为http://192.168.100.100:8087/springboot/...
		location ^~ /springboot/ {
			#请求转向global 定义的服务器列表
			proxy_pass   http://global;
		}
	}
}
```

注：192.168.100.100地址是博主瞎写的，读者可改为自己实际的IP地址

由于项目比较简单，所以也没有太多复杂的配置，接下来对Nginx负载均衡的一些基础知识做一下简单介绍。

#### 3、nginx的upstream的几种方式

	> 轮询（默认）

每个请求按照时间顺序逐一分配到不同的后端服务器，如果后端服务器冗机，能自动剔除。

> ip_hash

每个请求按访问ip的hash结果分配，这样每个访问固定访问一个后端服务器。

> weight

指定轮询几率，weight和访问比率成正比，用于后端服务器性能不均的情况。

> fair(第三方)

按后端服务器的响应时间来分配请求，响应时间短的优先分配。

> url_hash(第三方)

按访问URL的hash结果来分配请求，使每个URL定向到同一个后端服务器，后端服务器为缓存时比较适用。另外，在upstream中加入hash语句后，server语句不能写入weight等其他参数。



总结一下，负载均衡简单的理解其实可以看做是用户请求Nginx，Nginx将用户的请求URL按照配置的方式截取，然后按照配置的upstream的方式请求后端服务器。