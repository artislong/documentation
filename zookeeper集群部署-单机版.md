---
layout: post
title: zookeeper集群部署(单机版)
date: 2016-12-20 00:05:59
tags: zookeeper
categories: zookeeper入门
toc: true
description:
---
 
***本文是在机器已经配置好java开发环境的基础上完成的。***

### 软件准备
Zookeeper最新版本是3.4.6，笔者使用zookeeper-3.4.5-cdh5.3.6.tar.gz完成集群的搭建。
Zk的其他版本下载地址：[http://www.apache.org/dist/zookeeper/](http://www.apache.org/dist/zookeeper/)
将下载好的zookeeper解压，<!--more-->建议重命名为zookeeper-3.4.5-1，并且拷贝出相同的两份，分别命名为zookeeper-3.4.5-2，zookeeper-3.4.5-3。<p>
   然后在适当的目录下新建一个存放zookeeper数据的目录，笔者新建了devtools目录，在devtools目录中新建zookeeper-1目录，在其中增加data目录和log目录（这两个目录的作用在后面会做说明）；然后将zookeeper-1目录拷贝出两份，分别命名为zookeeper-2，zookeeper-3。<p>
笔者目录结构为：<p>
<a href="http://i1.piimg.com/4851/34885c8a253a8a22.png" title="点击显示原始图片"><img src="http://i1.piimg.com/4851/34885c8a253a8a22t.jpg"></a><p>
   到此，软件和环境准备工作已经做好，接下来就需要对zookeeper的相关配置文件做修改了。
### 集群配置
新下载的Zookeeper，conf目录下有configuration.xsl，log4j.properties，zoo_sample.cfg三个配置文件，我们需要将zoo_sample.cfg复制一份，命名为zoo.cfg，因为zookeeper启动的时候会读取zoo.cfg文件的配置信息。
zoo.cfg文件中有几项参数，主要解释一下三项参数：<br>
tickTime=2000  基本事件单元，以毫秒为单位。它用来控制心跳和超时，默认情况下最小的会话超时时间为两倍的 tickTime。<br>
clientPort=2181  监听客户端连接的端口。<br>
dataDir=/tmp/zookeeper  zookeeper默认存放数据的目录<p>
 
接下来我们需要修改zoo.cfg中的配置信息。
在zoo.cfg中增加这两项配置：

```
	dataDir=F:/hadoop/tools/devtools/zookeeper-1/data
	dataLogDir=F:/hadoop/tools/devtools/zookeeper-1/log
```
  这都是我们之前新建的目录，dataDir前面已经作过说明，dataLogDir是zookeeper存放日志文件的目录，如果不配置dataLogDir，日志文件默认存放到dataDir中。<p>
  然后配置zk进程（server.x=ip:clientPort:zkReqPort）：

```
    server.1=localhost:2287:3387        
    server.2=localhost:2288:3388
    server.3=localhost:2289:3389
```
  server.x中的“***x***”表示ZooKeeper Server进程的标识，同一个ZooKeeper集群内的ZooKeeper Server进程间的通信不仅可以使用具体的点IP地址，也可以使用组播地址。<p>
  ***clientPort*** 表示监听Client端请求的端口号<p>
  ***zkReqPort***  表示监听同ZooKeeper集群内其他ZooKeeper Server进程通信请求的端口号<p>
因为笔者是在同一台机器上作伪集群的搭建，所以各个zookeeper的ip相同，保证每个zk进程的监听端口错开就可以了，如果是在不同机器上，则不需要。<p>
以上配置已经完成了集群99%的搭建工作。<p>
 
  此时需要在每个zookeeper-1/data目录中新建myid文件，文件内容为1，然后在zookeeper-2/data，zookeeper-3/data目录中也同样新建myid文件，内容分别为2和3。1,2,3分别对应server.x中的x，表示zookeeper进程的进程标识。如果不配置zookeeper的进程标识，zk集群将不能找到自己所对应的进程。因此，之前99%的工作也不如此处1%的重要。<p>
自此，入门级集群的搭建基本完成。
### 集群测试
首先启动zk集群，windows下直接运行三个zookeeper服务器的zkServer.cmd即可。在启动第一个zookeeper的时候会出现以下错误信息：<p>
<a href="http://i1.piimg.com/4851/da2eac70e3af7468.jpg" title="点击显示原始图片"><img src="http://i1.piimg.com/4851/da2eac70e3af7468t.jpg"></a><br>
<p>这些错误信息不要害怕，因为集群其他进程没启动，检查线程没有找到其他的服务器节点。在启动第二个zookeeper服务器之后就正常了。<p>
 
启动成功之后就需要我们通过java代码去测试集群是否可用。<p>
 
新建一个maven工程，在pom.xml文件中添加一下依赖信息：

```
	<dependency>
    	<groupId>org.apache.zookeeper</groupId>
	    <artifactId>zookeeper</artifactId>
	    <version>3.4.6</version>
	    <exclusions>
	        <exclusion>
	            <groupId>org.slf4j</groupId>
	            <artifactId>slf4j-log4j12</artifactId>
	        </exclusion>
	        <exclusion>
	            <groupId>log4j</groupId>
	            <artifactId>log4j</artifactId>
	        </exclusion>
	    </exclusions>
	</dependency>
	<dependency>
	    <groupId>com.github.sgroschupf</groupId>
	    <artifactId>zkclient</artifactId>
	    <version>0.1</version>
	</dependency>
 ```
Zkclient是github上一个开源的java zk客户端。
编写测试代码

```
	@Test
	public void testZkClient() {
	    ZkClient zkClient = new ZkClient("localhost:2181,localhost:2182,localhost:2183");
	    String node = "/zookeeper-3.4.5-1";
	    if (zkClient.exists(node)) {
	        System.out.println(node);
	    }
	}
```
zkClient.exists(node) 检测zookeeper-3.4.5-1 是否存在
 
zookeeper集群的搭建及测试完成，在Linux下搭建伪集群和不同机器下搭建集群方式基本相同。笔者建议后来者可先在windows环境下搭建，windows的可视化界面效果能够为初学者减少很多出错的地方。